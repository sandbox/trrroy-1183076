GENERAL ESI OVERVIEW:

Akamai's ESI proxies will consume markup tags <esi:xxxx> </esi:xxxx>
and replace the tags with the actual website content based on the 
instructions/paramaeters in the esi tag. On the "proxy" version of the 
page you can view HTML source and you will not see any <esi> tags but if you 
view HTML source on the 'origin' server then you can see the <esi> tags and 
follow the src attribute to view the discreet ESI html fragment.  

INSTALLATION:

1. Install as normal module and then visit admin/settings/akamai_esi for set up

2. In your theme's page templages, replace: 'print $messages;' with 
'print (module_exists('petside_esi')) ? petside_esi_set_msg($messages) : $messages;' 

3. The module will handle the caching of blocks and views in your site but you 
will need to set your basic caching definitions with your Akamai representative.  


MODULE DETAILS:
 
This module hijacks any blocks and/or views specified at
admin/settings/akamai_esi. It also hijacks the display of drupal messages. 
When hijacked, the display is replaced with an Akamai ESI tag of the form:
  
  <esi:include src="/url/to/frag/goes/here" ttl="0s" onerror="continue" />

Also, if there is no HTTP header from the Akamai network then the <remove> 
tag will be added to the page as failover content in the event the origin 
server does not reply. This content also allows developers to view the page 
as intended on non-Akamai servers:

  <esi:remove> [block/view content goes in here] </esi:remove>

Then ESI will request the ESI fragment from the 'src' parameter in the esi 
tag above. The retrieved fragment is only the content for that block/view.

The blocks/views are rendered by using url args for module, block delta and 
view args. There should not be additional GET args added for nid or uid 
because this information causes a template to be cached with a <esi> fragment
specific to that nid or uid instead of letting Drupal do that work when the 
fragment is requested from the origin server. ESI src urls should be general.

A cookie is placed on login and reset on logout which is used by Akamai to 
set different TTL's for content for anon vs. authenticated users. These TTL's 
are defined on the admin settings page for this module at 
admin/settings/akamai_esi. 

The Akamai CCAPI is used to manage cache clearing. The Akamai account username 
and password need to be set at admin/settings/akamai_esi/api.


NOTE ABOUT ADMIN CONTENT AND CACHING:

This module detects HTTP headers from the Akamai network and uses 
admin_supress() from Drupal API to prevent admin menus from output to 
Akamai's CDN. 

*****
You can NOT use your Akamai'd site for admin functionality. You MUST use your 
'origin' site to use Drupal admin features.
***** 
 
